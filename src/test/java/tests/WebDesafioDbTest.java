package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WindowType;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class WebDesafioDbTest {
    @Test
    public void teste() throws InterruptedException {
        WebDriver driver = new ChromeDriver();
        String idGuiaTransferir = driver.getWindowHandle();

        String urlBugBank = "https://bugbank.netlify.app/";

        //Dados da conta - Conta responsável por transferir o dinheiro.
        String emailTransferir = "desafio.tranferir@teste.com";
        String nomeTransferir = "Desafio Transferir";
        String senhaTransferir = "abcd012345";

        //Dados da conta - Conta responsável por receber o dinheiro.
        String emailReceber = "desafio.receber@teste.com";
        String nomeReceber = "Desafio Receber";
        String senhaReceber = "abcd54321";
        String contaReceber;
        String digitoContaReceber;

        /* ***** Operação para registrar a conta que vai transferir ***** */
        driver.get(urlBugBank);
        this.regsitrarUsuario(driver, emailTransferir, nomeTransferir, senhaTransferir);

        /* ***** Acessar conta que vai transferir ***** */
        TimeUnit.MILLISECONDS.sleep(1200);
        driver.get(urlBugBank);
        this.acessarConta(driver, emailTransferir, senhaTransferir);

        /* ***** Operação para registrar a conta que vai receber ***** */
        TimeUnit.MILLISECONDS.sleep(1200);

        WebDriver driverRegistroContaRecebimento = driver.switchTo().newWindow(WindowType.TAB);
        String idGuiaReceber = driverRegistroContaRecebimento.getWindowHandle();

        driverRegistroContaRecebimento.get(urlBugBank);
        this.regsitrarUsuario(driverRegistroContaRecebimento, emailReceber, nomeReceber, senhaReceber);

        /* ***** Acessar conta que vai receber ***** */
        TimeUnit.MILLISECONDS.sleep(1200);

        driverRegistroContaRecebimento.get(urlBugBank);
        this.acessarConta(driverRegistroContaRecebimento, emailReceber, senhaReceber);

        TimeUnit.MILLISECONDS.sleep(1200);
        Map<String, String> dadosConta = this.recuperarDadosConta(driverRegistroContaRecebimento);
        contaReceber = dadosConta.get("conta");
        digitoContaReceber = dadosConta.get("digito");

        /* ****** Transferência ****** */
        driver.switchTo().window(idGuiaTransferir);
        driver.get(urlBugBank);
        this.acessarConta(driver, emailTransferir, senhaTransferir);

        TimeUnit.MILLISECONDS.sleep(1200);
        this.transferirDinheiro(driver, contaReceber, digitoContaReceber, "100");

        TimeUnit.MILLISECONDS.sleep(1200);
        driver.findElement(By.id("btn-EXTRATO")).click();

        /* ****** Validar valor creditado e debitado ****** */
        TimeUnit.MILLISECONDS.sleep(1200);

        driverRegistroContaRecebimento.switchTo().window(idGuiaReceber);
        driverRegistroContaRecebimento.get(urlBugBank);
        this.acessarConta(driverRegistroContaRecebimento, emailReceber, senhaReceber);

        TimeUnit.MILLISECONDS.sleep(1200);
        driverRegistroContaRecebimento.findElement(By.id("btn-EXTRATO")).click();

        TimeUnit.MILLISECONDS.sleep(1200);
        String saldoContaCredito = driverRegistroContaRecebimento.findElement(By.id("textBalanceAvailable")).getText();
        driver.switchTo().window(idGuiaTransferir);
        String saldoContaDebito = driver.findElement(By.id("textBalanceAvailable")).getText();

        Assertions.assertEquals("R$ 1.100,00", saldoContaCredito);
        Assertions.assertEquals("R$ 900,00", saldoContaDebito);
    }

    private void regsitrarUsuario(WebDriver navegador, String email, String nome, String senha) throws InterruptedException {
        //Botão para registrar usuário.
        navegador.findElement(By.xpath("//*[@id=\"__next\"]/div/div[2]/div/div[1]/form/div[3]/button[2]"))
                .click();

        TimeUnit.MILLISECONDS.sleep(1200);
        navegador.findElement(By.xpath("//*[@id=\"__next\"]/div/div[2]/div/div[2]/form/div[2]/input"))
                .sendKeys(email);
        navegador.findElement(By.name("name"))
                .sendKeys(nome);
        navegador.findElement(By.xpath("//*[@id=\"__next\"]/div/div[2]/div/div[2]/form/div[4]/div/input"))
                .sendKeys(senha);
        navegador.findElement(By.name("passwordConfirmation"))
                .sendKeys(senha);
        navegador.findElement(By.id("toggleAddBalance"))
                .click();
        navegador.findElement(By.xpath("//*[@id=\"__next\"]/div/div[2]/div/div[2]/form/button"))
                .click();
    }

    private void acessarConta(WebDriver navegador, String email, String senha) throws InterruptedException {
        navegador.findElement(By.name("email"))
                .sendKeys(email);
        navegador.findElement(By.name("password"))
                .sendKeys(senha);

        TimeUnit.MILLISECONDS.sleep(1200);
        navegador.findElement(By.xpath("//*[@id=\"__next\"]/div/div[2]/div/div[1]/form/div[3]/button[1]"))
                .click();
    }

    private Map<String, String> recuperarDadosConta(WebDriver navegador) {
        String numeroConta = navegador.findElement(By.id("textAccountNumber")).findElement(By.tagName("span")).getText();

        Map<String, String> dadosConta = new HashMap<>();
        dadosConta.put("conta", numeroConta.substring(0, numeroConta.length() - 2));
        dadosConta.put("digito", String.valueOf(numeroConta.charAt(numeroConta.length()-1)));

        return dadosConta;
    }

    private void transferirDinheiro(WebDriver navegador, String contaReceber, String digitoContaReceber, String valorTransferencia) throws InterruptedException {
       //Ação para entrar na tela de transferência.
        navegador.findElement(By.xpath("//*[@id=\"btn-TRANSFERÊNCIA\"]")).click();

        TimeUnit.MILLISECONDS.sleep(1500);
        navegador.findElement(By.name("accountNumber")).sendKeys(contaReceber);
        navegador.findElement(By.name("digit")).sendKeys(digitoContaReceber);
        navegador.findElement(By.name("transferValue")).sendKeys(valorTransferencia);
        navegador.findElement(By.name("description")).sendKeys("Transferência para Desafio Receber");

        //Ação para transferir o dinheiro.
        TimeUnit.MILLISECONDS.sleep(1500);
        navegador.findElement(By.xpath("//*[@id=\"__next\"]/div/div[3]/form/button")).click();

        //Fechar modal de confirmação da transferência.
        TimeUnit.MILLISECONDS.sleep(1500);
        navegador.findElement(By.id("btnCloseModal")).click();

        //Voltar para tela inicial do banco com o usuário logado.
        TimeUnit.MILLISECONDS.sleep(1500);
        navegador.findElement(By.id("btnBack")).click();
    }
}
